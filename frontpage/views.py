from io import BytesIO

from PIL import Image
from django.contrib.auth.models import User
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, redirect
from django.views import generic
from .forms import NewUserForm
from .models import Cart, Order, Product, Components, Profile
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
import decimal

# Create your views here.
# def index(request):
#     return render(request, os.path.join(os.path.abspath(os.getcwd()), r'frontpage\templates\index.html'), {})


# class ProductList(generic.ListView):
#     queryset = Product.objects.order_by('-price')
#     template_name = 'index.html'


def ProductList(request):
    product_list = Product.objects.order_by('-price')

    return render(request, 'index.html', {
        'products': product_list,
    })


def PaginatedProductList(request):
    page = request.GET.get('page', '')
    product_list = Product.objects.all()
    paginator = Paginator(product_list, 10)
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)

    return render(request, 'shop.html', {
        'products': products,
    })

def add_to_cart(request, id):
    product = Product.objects.get(id=id)
    if product.stock != 0:
        if not 'cart' in request.session or not request.session['cart']:
            request.session['cart'] = [id]
            request.session['qtd'] = [1]
        else:
            cart_itens = request.session['cart']
            qtd_itens = request.session['qtd']
            if id in cart_itens:
                idx = cart_itens.index(id)
                if product.stock > qtd_itens[idx]:
                    qtd_itens[idx] += 1
            else:
                cart_itens.append(id)
                qtd_itens.append(1)
            request.session['cart'] = cart_itens
            request.session['qtd'] = qtd_itens
    return redirect('cart')

def remove_from_cart(request,id):
    cart_itens = request.session['cart']
    idx = cart_itens.index(id)
    cart_itens.pop(idx)
    request.session['cart'] = cart_itens
    return redirect('cart')


def CartView(request):
    if not 'cart' in request.session or not request.session['cart']:
        return render(request, 'cart.html', {
            'result': False
        })
    products_id = request.session['cart']
    products = []
    qtd_products = request.session['qtd']
    totals = []
    for id, qtd in zip(products_id, qtd_products):
        product = Product.objects.get(id=id)
        products.append(product)
        totals.append(product.price * qtd)
    result = True
    if len(products) == 0:
        result = False

    return render(request, 'cart.html', {
        'products': zip(products, qtd_products, totals),
        'result': result
    })


def CheckoutView(request):
    if request.user.is_authenticated:
        products_id = request.session['cart']
        products = []
        qtd_products = request.session['qtd']
        totals = []
        product_total = 0
        for id, qtd in zip(products_id, qtd_products):
            product = Product.objects.get(id=id)
            products.append(product)
            total = product.price * qtd
            totals.append(total)
            product_total += total
        result = True
        if len(products) == 0:
            result = False

        decimal.getcontext().prec = 2
        shipping = decimal.Decimal(10.00)
        subtotal = product_total + shipping
        profile = Profile.objects.get(user=request.user)
        return render(request, 'checkout.html', {
            'products': zip(products, totals),
            'result': result,
            'product_total': product_total,
            'subtotal': subtotal,
            'shipping': shipping,
            'user_profile': profile
        })
    else:
        request.session['redirect'] = 'checkout'
        return redirect('login')


class ShopView(generic.ListView):
    queryset = Product.objects.order_by('-price')
    template_name = 'shop.html'


def detail(request, id):
    requested = Product.objects.get(id=id)
    resized_im1 = resize(requested.im1)
    resized_im2 = resize(requested.im2)
    requested.im1 = resized_im1
    requested.im2 = resized_im2
    return render(request, 'detail.html', {
        'product': requested,
    })


def resize(image_file):
    import base64
    image_decoded = base64.b64decode(image_file)
    im_file = BytesIO()
    img = Image.open(BytesIO(image_decoded))
    w, h = img.size
    ratio = 1
    if h > 1204:
        ratio = 540*1./h
    new_size = img.resize((int(w*ratio), int(h*ratio)))
    new_size.save(im_file, format="JPEG")
    im_bytes = im_file.getvalue()
    encoded_string = base64.b64encode(im_bytes)
    base64_string = encoded_string.decode('ascii')
    return base64_string


class ContactView(generic.ListView):
    queryset = Product.objects.order_by('-price')
    template_name = 'contact.html'


def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"Você logou como: {username}.")
                if request.user.is_superuser:
                    return redirect("/admin")
                else:
                    if not 'redirect' in request.session or not request.session['redirect']:
                        return redirect('index')
                    else:
                        location = request.session['redirect']
                        request.session['redirect'] = None
                        return redirect(location)

            else:
                messages.error(request, "Usuario ou senha inválidos.")
        else:
            messages.error(request, "Usuario ou senha inválidos.")
    if request.user.is_authenticated:
        if request.user.is_superuser:
            return redirect("/admin")
        else:
            return redirect("index")

    form = AuthenticationForm()
    return render(request=request, template_name="login.html", context={"login_form": form})


def register(request):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registration successful.")
            return redirect("index")
        messages.error(request, "Unsuccessful registration. Invalid information.")
    form = NewUserForm()
    return render(request=request, template_name="register.html", context={"register_form": form})


def user_logout(request):
    logout(request)
    return redirect("index")
