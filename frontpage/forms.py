from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from frontpage.models import Profile
from frontpage.widgets import DatePickerInput


class NewUserForm(UserCreationForm):
    email = forms.EmailField(label='E-mail', required=True)
    nome = forms.CharField(label='Nome', required=True)
    sobrenome = forms.CharField(label='Sobrenome', required=True)
    nascimento = forms.DateField(widget=DatePickerInput(), label='Data de nascimento', required=True)
    endereco = forms.CharField(label='Endereço', required=True)
    numero = forms.CharField(label='Número', required=True)
    complemento = forms.CharField(label='Complemento', required=True)
    cidade = forms.CharField(label='Cidade', required=True)
    uf = forms.CharField(label='UF', required=True)
    cep = forms.CharField(label='CEP', required=True)

    class Meta:
        model = User
        fields = ("nome", "sobrenome", "nascimento", "email", "password1", "password2", "endereco",
                  "numero", "complemento", "cidade", "uf", "cep")

    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.username = self.cleaned_data['email']
        user.first_name = self.cleaned_data['nome']
        user.last_name = self.cleaned_data['sobrenome']
        profile = Profile()
        profile.location = self.cleaned_data['endereco']
        profile.number = self.cleaned_data['numero']
        profile.location_complement = self.cleaned_data['complemento']
        profile.city = self.cleaned_data['cidade']
        profile.state = self.cleaned_data['uf']
        profile.zip_code = self.cleaned_data['cep']
        profile.birth_date = self.cleaned_data['nascimento']

        if commit:
            user.save()

        profile.user = user

        if commit:
            profile.save()

        return user


