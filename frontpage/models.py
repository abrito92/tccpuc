from io import BytesIO

from PIL import Image
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import pre_save

# Create your models here.
fs = FileSystemStorage(location='images')


class Product(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField()
    summary = models.TextField()
    description = models.TextField()
    stock = models.IntegerField()
    price = models.DecimalField(max_digits=999, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    im1 = models.TextField()
    im2 = models.TextField()
    image_1 = models.ImageField(upload_to='images', blank=True, null=False)
    image_2 = models.ImageField(upload_to='images', blank=True, null=False)

    @property
    def nome(self):
        return self.name

    class Meta:
        ordering = ['-price', '-created_at']


def image_to_b64(image_file):
    import base64
    im_file = BytesIO()
    image_file.save(im_file, format="JPEG")
    im_bytes = im_file.getvalue()
    encoded_string = base64.b64encode(im_bytes)
    base64_string = encoded_string.decode('ascii')
    return base64_string


@receiver(pre_save, sender=Product)
def create_base64_str(sender, instance=None, **kwargs):
    instance.im1 = image_to_b64(Image.open(instance.image_1))
    instance.im2 = image_to_b64(Image.open(instance.image_2))


class Banner(models.Model):
    image = models.ImageField(unique=True, upload_to='images', blank=True, null=True)


class Order(models.Model):
    metadata = models.TextField()
    metadata_type = models.IntegerField()
    total_value = models.FloatField()
    status = models.IntegerField()
    ship_to = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)


class Cart(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    itens_qty = models.IntegerField()


class Components(models.Model):
    name = models.CharField(max_length=100)
    type = models.IntegerField()
    description = models.TextField()
    stock = models.IntegerField()
    price = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.CharField(max_length=30, blank=True)
    number = models.CharField(max_length=30, blank=True)
    location_complement = models.CharField(max_length=30, blank=True)
    city = models.CharField(max_length=200, blank=True)
    state = models.CharField(max_length=30, blank=True)
    zip_code = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)

    @property
    def nome(self):
        return self.user.name


class Categories(models.Model):
    pass

class Steels(models.Model):
    pass