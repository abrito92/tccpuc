from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as Admin
from django.contrib.auth.models import User

from .models import Cart, Order, Product, Components, Profile


# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'stock', 'price', 'created_at')
    search_fields = ['name', 'price']
    exclude = ('im1', 'im2', )


class ProfileInline(admin.StackedInline ):
    model = Profile
    can_delete = False
    verbose_name_plural = 'profiles'


class UserAdmin(Admin):
    inlines = (ProfileInline,)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Cart)
admin.site.register(Order)
admin.site.register(Product, ProductAdmin)
