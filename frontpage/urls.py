from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('', views.ProductList, name='index'),
    path('products/<int:id>/', views.detail, name='product_detail'),
    path('cart', views.CartView, name='cart'),
    path('checkout', views.CheckoutView, name='checkout'),
    path('shop', views.PaginatedProductList, name='shop_home'),
    path('contact', views.ContactView.as_view(), name='contact'),
    path('login', views.login_request, name="login"),
    path('register', views.register, name="register"),
    path('logout/', views.user_logout, name="logout"),
    path('add-to-cart/<int:id>/', views.add_to_cart, name="add_to_cart"),
    path('remove-from-cart/<int:id>/', views.remove_from_cart, name="remove_from_cart"),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)